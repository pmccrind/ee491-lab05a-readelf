/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491 - Spr 2022
///
/// @file    readelf.c
/// @version 0.1 - copied from wc.c
///
/// readelf - print out the readelf header information
//
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    20_Mar_2022
///
/// @see    https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include "readelf.h"

// Stores what type of machine we are on
bool endianMachine;

// Need Argc and argv
// @argc - argument count passed to the program, expected 1 or more
// @argv - argument list passed to the program, expected 
// ["./readelf", "somefile.txt", "-h"]
int main( int argc, char *argv[] ) {
   
   //Commandline Parsing will be completed here


   // No File Provided read from stdin
   // No flags either provide standard output
   // Return error
   if (argc == 1) {
      
      //File did not open successfully
      fprintf(stderr, "%s: Warning nothing to do.\n", FILENAME);
      
      // @TODO print out the avialable flags and description of them

      //Exit with failure
      exit(EXIT_FAILURE);


   } else {
      // Two or more arguments recieved, need to find all valid flag commandline
      // pass to parse commandline argument parser.
   
      Arguments returnedArgs;

      returnedArgs = parseCommandLineArguments( argc, argv );      

      //Check to make sure the h flag is true
      // @TODO add in checks for all future flags
      if ( !returnedArgs.hFlag ){
         fprintf( stderr, "%s: the -h flag is required.\n", FILENAME );
         //exit with failure
         exit(EXIT_FAILURE);

      }
      
      // header flag set need to make sure the file supplied is an elf file
      

      // Check to see what type of machine we are runnning on

      endianMachine = endianCheck();

      // open each of the files returned by the parsing function
      if ( returnedArgs.filed == 0  ){
         
         //No files returned error and exit
         fprintf( stderr, "%s: Warning nothing to do. \n", FILENAME );
         //exit with failure
         exit( EXIT_FAILURE );
      }
      
      // String used to check if the program is a readelf program or
      // not.
      char elfString[] = {0x7f, 0x45, 0x4c, 0x46,  0x00};
         
      // Stores the Constant elf in the String
      char elfConstant[8];
       
      // Converts the second string and stores in the first
      stringToHex( elfConstant, elfString, sizeof(elfString) - 1 );   
      
      // For comparing to the number of files returned
      int filing = 0;

      //File pointer for the incoming files
      FILE* fileptr;
     
      // Stores the files name
      char filename[512];

      // Open all of the files found with getopt().
      while ( filing < returnedArgs.filed  ) {
         
         // Copy from the returned array in the struct to 
         // file for opening.
         strcpy( filename, returnedArgs.files[filing] );
         
         fileptr = fopen ( filename, "r" );

         // File couldn't be opened
         if ( fileptr == NULL ){
         
            fprintf( stderr, "%s: File [%s] could not be opened for processing.\n", FILENAME, filename);
            
            // Increment loop
            filing++;

            // Continue to try and open other files if any
            continue;
         }// End null file pointer check 

   
         // Check to see if there is more than one file to be opened
         if ( returnedArgs.filed >= 2 ){
            // Print out the file name
            printf( "\nFile: %s\n", filename );
         }
         
         // Parse the File and determine the type either 32 or
         // 64.
         int fileType = parseAndCheckFile( fileptr );

         if ( fileType == 64 ) {
            // Make a 64 bit struct
            ElfFile64 elf64;

            // Store the information from the file into
            // the struct.            
            elf64 = parseAndStore64 ( fileptr ); 
            
            // Print out the information
            print64( &elf64 );

         } else if ( fileType == 32) {
            // Make a 32 bit struct
            ElfFile32 elf32;

            // Store the information from the file into
            // the struct.
            elf32 = parseAndStore32 ( fileptr );
           
            // Print out the information
            print32 ( &elf32 );

         } else if ( fileType == 1) {

            // Not an Elf File
            fprintf ( stderr, "%s: [%s] is not an elf file.\n", FILENAME, filename );

            // Increment
            filing++;

            // Continue to the next file if any
            continue;

         } else {
            // Unknown value for type
            filing++;
            
            // Print Error with type of file

            fprintf ( stderr, "%s: Incorrect file type, received [%d] and only 1 or 2 are allowed.\n", FILENAME, fileType );

            // Continue to next file if any
            continue;

         }


         //close the file
         int fileClosed = fclose( fileptr );

         if ( fileClosed != 0 ){
            //error with closing
            fprintf( stderr, "%s: File [%s] could not be closed properly. \n", FILENAME, filename );
            // exit with failure
            exit( EXIT_FAILURE );
         }

         //File processed successfully increment loop and head back to the top
         filing++;
      }//end loop over files

      //Exit with success
      exit(EXIT_SUCCESS);

   }//end argc >= 2 case
      
   //Should not reach this section,
   //exit with failure if you do.
   exit(EXIT_FAILURE);

}//End Main Function

