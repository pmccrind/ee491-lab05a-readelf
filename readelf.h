/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491 - Spr 2022
///
/// @file    readelf.h
/// @version 0.5 - added system lookup declarition
///
/// readelf - display the information from headers of programs
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    20_Mar_2022
///
///////////////////////////////////////////////////////////////////////////////

#define VERSION "\n readelf 1.0 \n Copyright Patrick McCrindle\n"
#define FILENAME "readelf"


// Struct to hold all the information about a 64
// bit elf file.
typedef struct {
   
   // First 13 bytes.
   char magic[40];

   // Hold the class number, will always be
   // 2.
   int classNum;

   // Little or big endian
   int littleEndian;

   // Hold the version, always 1
   int version;

   // Hold the identity
   char identity[40];

   // Hold the ABI version, should always
   // be 0.
   int elfABI;

   // next 7 all 0's
   
   // Hold the type String
   char elfType[60];

   // Hold the machine string
   char elfMachine[60];

   // Hold the version, always 1
   long int elfVersion;

   // Different sizes below for 64 bit struct

   // Holds the entry point
   char elfEntry[40];

   // Holds the start of the program header
   char elfProgram[40];

   // Holds the start of the section header
   long long int elfHeader;

   // Different sizes above for 64 bit struct
   
   // Hold flags that are set
   char elfFlags[20];

   // Hold the header size, 64 for this struct
   char elfSize[15];

   // Hold size of the program header
   long int elfProgramSize;

   // Hold number of entries in the program header
   long int elfProgramHeaderNumber;

   // Hold size of the section header
   long int elfSectionHeaderSize;

   // Hold number of entries in the program header
   long int elfSectionHeaderEntries;

   // Holds index of the Section header table 
   // entry.
   long int elfIndexOfSection;


} ElfFile64;



// Struct to hold all the information about a 32
// bit elf file.
typedef struct {
   
   // First 13 bytes.
   char magic[40];

   // Hold the class number, will always be
   // 2.
   int classNum;

   // Little or big endian
   int littleEndian;

   // Hold the version, always 1
   int version;

   // Hold the identity
   char identity[40];

   // Hold the ABI version, should always
   // be 0.
   int elfABI;

   // next 7 all 0's
   
   // Hold the type String
   char elfType[60];

   // Hold the machine string
   char elfMachine[60];

   // Hold the version, always 1
   long int elfVersion;


   // Different sizes below for 32 bit struct

   // Holds the entry point
   char elfEntry[40];

   // Holds the start of the program header
   char elfProgram[40];

   // Holds the start of the section header
   long long int elfHeader;

   // Different sizes above for 32 bit struct
   
   // Hold flags that are set
   char elfFlags[20];

   // Hold the header size, 64 for this struct
   char elfSize[15];

   // Hold size of the program header
   long int elfProgramSize;

   // Hold number of entries in the program header
   long int elfProgramHeaderNumber;

   // Hold size of the section header
   long int elfSectionHeaderSize;

   // Hold number of entries in the program header
   long int elfSectionHeaderEntries;

   // Holds index of the Section header table 
   // entry.
   long int elfIndexOfSection;



} ElfFile32;


// Struct to pass the arry of files and the flags
// returned from parseCommandLineArguments
typedef struct {
   
   //start of flag list
   bool hFlag;

   // Array for list of files found by getopt()
   // Create an array of strings
   // @TODO notify users of the 10 file limit.
   char files[10][512];

   //number of files in files
   int filed;

} Arguments;


// Function header for parsing commandline arguments
// argc, number of arguments from main
// argv[] array of strings of all the commandline
// arguments
Arguments parseCommandLineArguments( int argc, char* argv[] );

// Parses and does check to see if the file is a 32 
// or 64 bit file.
int parseAndCheckFile( FILE* incomingFile );

// Function to convert the given string to hex
// needs a string and modifys the given string.
void stringToHex ( char* hexString, char* incomingString, int length );


// Check which type the incoming hex string matches
// for the given type. Returns in the second parameter.
void typeCheck ( char* hexString, char* returnString );


// Parses a 64 bit elf header file and stores the values
// as hex strings into an ElfFile64 struct.
ElfFile64 parseAndStore64 ( FILE* incomingFile ); 

// Parses a 32 bit elf header file and stores the values
// as hex strings into an ElfFile32 struct.
ElfFile32 parseAndStore32 ( FILE* incomingFile ); 


// Check which machine the incoming hex string matches
// for the given mchine. Returns in the second string.
void machineCheck ( char* hexString, char* returnString );


// Function used to see if readelf is being run on a
// little or big endian machine.
bool endianCheck();


// Swaps the chars in the string to the correct order
void swap( char* swapString );


// Prints out all information from a 64 bit struct
void print64 ( ElfFile64* incomingElf );

// Prints out all information from a 32 bit struct
void print32 ( ElfFile32* incomingElf );



