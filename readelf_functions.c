////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05a - readelf - EE 491F - Spr 2022
///
/// @file    readelf_functions.c
/// @version 1.4 - added machineCheck function
///
/// readelf_functions - Holds required functions for readelf
///
/// @author  Patrick McCrindle <pmccrind@hawaii.edu>
/// @date    20_Mar_2022
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <stdlib.h>
#include <stdbool.h>
#include "readelf.h"

// Check what the endian is of the machine that the program is 
// running on. Stored in a global in readelf.c
bool endianCheck(){

   int endian = 1;

   // From Stack Overflow
   // https://stackoverflow.com/questions/4181951/how-to-check-whether-a-system-is-big-endian-or-little-endian/
   // Is true on little endian as the 1 is stored in endian
   // Is false on big endian as the 0 is stored in endian
   if ( *( char* )&endian == 1   ){
      //Little Endian
      
      return true;

   } else {
      //Big Endian
      
      return false;

   }

}// End the endianCheck function

// Swaps the order of the chars in a string.
void swap( char* swapString){
   
   int size = strlen(swapString);

   int i = 0;

   char c = ' ';
   char d = ' ';
   
   if ( size % 4 != 0){
      // Cant swap
      printf("Can't swap string\n");
      return;

   }

   for( i = 0; i < size; i += 4 ){
      c = swapString[i];
      d = swapString[i+1]; 

      swapString[ i ] = swapString[ i + 2 ];
      swapString[ i + 1 ] = swapString[ i + 3];
      swapString[ i + 2 ] = c;
      swapString[ i + 3 ] = d; 

   }

   return;
}


// Function to do commandline parsing.
// Using hint provided in the lab manual to use
// getopt.
// Current returns true if h flag is set and false
// otherwise.
Arguments parseCommandLineArguments( int argc, char* argv[] ){

   int getOpt = 0;
   

   // Structure to store the information to be returned
   // Struct is 0'd out
   Arguments arg = { false, { " " }, 0 };

   // getopt returns -1 when there are no more commands
   // to parse
   // The "h" will need to be changed for all the flags
   // that are to be checked and set by getopt.
   // For flags that need a option place a ':' after that
   // flag.
   // @TODO add in other flags
   while ( ( getOpt = getopt( argc, argv, ":h") ) != -1  ) {

      // switch not needed for 1 check but built for future
      // needed checks
      switch( getOpt  ){
         
         // check for h flag
         case 'h':
            arg.hFlag = true;
            break;
         
         case '?':
            // not a valid argument
            fprintf( stderr, "Unknown argument [%c]\n", optopt );
            // exit
            exit(EXIT_FAILURE);

         case ':':
            // missing argument for h flag 
            fprintf ( stderr, "Missing an argument for %c\n", optopt );
            // exit
            exit(EXIT_FAILURE);
      
      }//end switch

   }// end while loop

   
   // Used to hold the number of potential files to open
   int i = 0;
   // places the files from getopt() into the array in the
   // args struct
   while ( optind < argc  ){
      
      
      strcpy( arg.files[i], argv[optind++] );
      // Increase i to store in the next position
      // I needs to be increased before we increment
      // filed in the struct
      i++;
      // Increase the count for the number of files
      arg.filed = i;
   }//end while
   
   return arg;
}// end parseCommandLineArguments Function
 
 



// Convert the given String to its hex values
void stringToHex ( char* hexString, char* incomingString, int length ){
   
   // For loop
   int i = 0;
   // Hex String
   int j = 0;
   
   // Credit to Gene's answer on the stack overflow article below
   //https://stackoverflow.com/questions/46210513/how-to-convert-a-string-to-hex-and-vice-versa-in-c
   for ( i = 0; i < length; i++ ){
            
      sprintf( hexString + j, "%02x", incomingString[i] & 0xff );
      j += 2;
   
   }
   // Add in the null character at the end of the string


   hexString[j] = '\0';   
   // String has converted to hex

}// End String to Hex Function



// Parses the File and checks to see if the file
// is a a readElf File. If it is returns if it
// is 32 or 64 bit.
int parseAndCheckFile( FILE* incomingFile ){
   
   // Set the file to the beginning of the file
   fseek( incomingFile, 0, SEEK_SET );
   
   // String used to check if the program is a readelf program or
   // not.
   char elfString[] = {0x7f, 0x45, 0x4c, 0x46,  0x00};
         
   // Stores the Constant elf in the String
   char elfConstant[8];
       
   // Converts the second string and stores in the first
   stringToHex( elfConstant, elfString, sizeof(elfString) - 1 );     

   // Holds char that we read in
   char c = ' ';

   // For loop
   int i = 0;

   // Hold the first 4 chars;
   char stringCheck[5];

   // Get the first 4 bytes to check if it is a read
   // elf file.
   for ( i = 0; i < 4; i++ ){

      c = getc( incomingFile );
      
      stringCheck[i] = c;

   }

   // Append the 0 at the end of the string
   stringCheck[i] = '\0';

   // Stores string from file in Hex
   char hexFromFile[5];

   stringToHex ( hexFromFile, stringCheck, sizeof( stringCheck) - 1 );

   if ( ( strcmp( hexFromFile, elfConstant)) != 0){
      
      // Rewind the file pointer
      fseek( incomingFile, 0, SEEK_SET );

      // Return 0 to indicate not a readelf file
      // to move to the next file.
      return 1; 
   }

   // Is a read elf file figure out if 32 or 64 bit
   
   // Left off the read at the 4th byte, the 5th
   // contains the byte to determine which type 
   // the file is.
   c = fgetc ( incomingFile );

   if ( c == 1 ){


      // Rewind the file pointer
      fseek( incomingFile, 0, SEEK_SET );
      
      // 32 bit File
      return 32;
      
   } else if ( c == 2) {

      // Rewind the file pointer
      fseek( incomingFile, 0, SEEK_SET );
      
      // 64 bit file
      return 64;
   
   } else {
      
      // Rewind the file pointer
      fseek( incomingFile, 0, SEEK_SET );
      
      // Error
      return c;     
   }

   // Shouldn't get here return 0 for error
   // if you do.
   return 0;

} // End the parse and Check file function

// Takes an incoming file pointer and stores 
// the information into an ElfFile64 struct. 
// Definition of ElfFile64 is in readelf.h
ElfFile32 parseAndStore32 ( FILE* incomingFile ){
   
   ElfFile32 elf;

   // Make sure the file is at the start of the file
   fseek( incomingFile, 0, SEEK_SET );

   // For loop
   int i;

   // Get from file char by char
   char c = ' ';

   // String to hold the magic
   char magicFile[14];

   // Get the magic out of the file
   for ( i = 0; i < 13; i++){
      c = fgetc( incomingFile );
      magicFile[i] = c;
   }
   
   // Append the 0 char at the end
   magicFile[i] = '\0';

   // Save into the struct the string
   strcpy( elf.magic, magicFile);

   // Store the Class number which we know is 2
   // As this is a 64 bit file
   elf.classNum = 2;


   // Figure out if the file is big or little endian.
   if ( elf.magic[5] == 1 ){
      //Little Endian
      elf.littleEndian = 1;
   } else if (elf.magic[5] == 2 ) {
      // Big Endian

      elf.littleEndian = -1;
   } else {
      // Not Endian Type
      fprintf( stderr, "%s: [%d] is not a valid Endian type.\n", FILENAME, elf.magic[5]);
      
      exit ( EXIT_FAILURE );
   } // End Endian Check

   
   // Check the Version number, should always be 
   // 1.
   if ( elf.magic[6] != 1){
      // Not Verison 1, exit with failure
      fprintf( stderr, "%s: [%d] is not a valid verison.\n", FILENAME, elf.magic[6] );
      
      exit ( EXIT_FAILURE );
 
   }
   
   // Version is 1, set it in the struct.
   elf.version = 1;

   // OS/ABI Check
   // @TODO Write a function to return the correct value
   // for the struct.
   if ( elf.magic[7] == 0){

      // Store the identity as a string
      char identity[] = "Unix System-V";
      
      // Store in the Struct
      strcpy ( elf.identity, identity );
      
   } else if ( elf.magic[7] == 1){
      
      // Store the identity as a string
      char identity[] = "HP-UX";
      
      // Store in the Struct
      strcpy ( elf.identity, identity );

   } else {
             // @TODO add in the rest of the checks for the rest of the
             // systems.

      // Store the identity as a string
      char identity[] = "Unknown";
      
      // Store in the Struct
      strcpy ( elf.identity, identity );


   }// Only checking for the first 2 values



   // Check the ABI Version
   if ( elf.magic[8] != 0 ) {

      // Error Not allowed
      fprintf ( stderr, "%s: ABI supported version is only 0. Found [%d] instead.\n", FILENAME, elf.magic[8] );

      // Exit
      exit( EXIT_FAILURE );
   } 

   // ABI version is 0
   elf.elfABI = 0;

   // Get the next 4 bytes for the type
   c = ' ';

   // Holds the bytes for the type
   char type[5];
   for ( i = 0; i < 4; i++){
      c = fgetc( incomingFile );
      type[i] = c;
   }
   
   // Add the null character
   type[i] = '\0';

   char typeAsHex[ 5 ];

   stringToHex( typeAsHex, type, sizeof( type ) - 1 );


   // Type Check return in the struct
   typeCheck( typeAsHex, elf.elfType );
   
   // Get the Machine Type
   c = ' ';
   
   // Store the machine type
   char machine[3];

   for ( i = 0; i < 2; i++ ){
      c = fgetc( incomingFile );
      machine[i] = c;
   }
  
   // Append null character
   machine[i] = '\0';


   // Hold the converted type as a hex value
   char machineAsHex[3];

   // Convert to Hex
   stringToHex( machineAsHex, machine, sizeof(machine) - 1 );


   // Compare the machine as a hex value against the list of known
   // machines. Return in the struct.
   machineCheck( machineAsHex, elf.elfMachine );


   // Get the Version
   c = ' ';

   // Store the Version
   char version[5];
   int j = 2;
   for ( i = 0; i < 2; i++){
      c = fgetc( incomingFile );
      version[j] = c;
      j++;
      c = fgetc ( incomingFile);
      version[j] = c;
      j = j - 3;
   }

   version[4] = '\0';
   // Store the Hec version 
   char hexVersion[5];

   // convert the string
   stringToHex( hexVersion, version, sizeof( version) - 1 );
  
  
   char one[5] = {0x00, 0x00, 0x00, 0x01, 0x00};

   char hexOne[5];

   stringToHex ( hexOne, one, sizeof( one ) - 1 );


   // Compare the Strings if not equal then error
   if ( ( strcmp( hexOne, hexVersion) ) != 0 ){

      // If not Zero wrong version and go to next file
      fprintf( stderr, "%s: version [%s] not supported.\n", FILENAME, hexVersion );

      exit ( EXIT_FAILURE );
   }
   
   //Version is 1 store in struct
   elf.elfVersion = strtol( hexVersion, NULL, 16 );

   // Get the entry point
   c = ' ';
   i = 0;
   // Hold Entry point String]
   char entry[9];
   j = 6;

   // Clear the last 0
   c = fgetc( incomingFile );
   for ( i = 0; i < 4; i++){
      c = fgetc( incomingFile );
      entry[j] = c;
      j++;
      c = fgetc( incomingFile );
      entry[j] = c;
      j = j - 3;
   }
   
   // Append null character
   entry[8] = '\0';
   char hexEntry[9];

   stringToHex( hexEntry, entry, sizeof( entry ) - 1 );
   
   // Swap the order to match little endian 
   swap(hexEntry);

   // Store in the Struct
   strcpy( elf.elfEntry, hexEntry );

   // Get the Program header Start Address
   c = ' ';

   // Store the program header string
   char program[9];
   j = 6;
   for (i = 0; i < 4; i++ ){
      c = fgetc( incomingFile );
      program[j] = c;
      j++;
      c = fgetc ( incomingFile );
      program[j] = c;
      j = j - 3;

   }
   
   // Append the null character
   program[8] = '\0';

   // Hold the hex version of the string
   char hexProgram[9];

   stringToHex( hexProgram, program, sizeof( program ) - 1 );
   
   // Swap the order
   swap(hexProgram);
   // Store into the Struct
   strcpy( elf.elfProgram, hexProgram );


   // Get the program header start
   c = ' ';
   // Store the header  
   char header[9];
   j = 6;
   for ( i = 0; i < 4; i++){
      c = fgetc( incomingFile );
      header[j] = c;
      j++;
      c = fgetc( incomingFile );
      header[j] = c;
      j = j - 3;
   }
   // Append the null character
   header[8] = '\0';

   // Hold the header in hex
   char hexHeader[9];

   stringToHex( hexHeader, header, sizeof( header) - 1 ); 
   
   swap(hexHeader);
   
   // Store in the struct as a long long
   elf.elfHeader = strtoll( hexHeader, NULL, 16);

   // Get the Flags Section 
   c = ' ';
   // Store the Flags
   char flags[5];
   j = 2;
   for ( i = 0; i < 2; i++){
      c = fgetc ( incomingFile );
      flags[j] = c;
      j++;
      c = fgetc( incomingFile );
      flags[j] = c;
      j = j - 3;
   }  
   
   // Append the null character
   flags[4] = '\0';

   char hexFlags[5];

   // Convert to Hex
   stringToHex( hexFlags, flags, sizeof( flags ) - 1 );

   // Store in the struct
   strcpy( elf.elfFlags, hexFlags );   

   // Get the Size of header should be 0x40
   c = ' ';
   char headSize[3];
   headSize[1] = fgetc ( incomingFile);

   headSize[0] = fgetc ( incomingFile);

   // Append the 0 character
   headSize[2] = '\0';
   

   
   // Convert to Hex Store in the Struct
   stringToHex( elf.elfSize, headSize, sizeof( headSize ) - 1 );
   
   
   // Get the Program Header Size
   c = ' ';
   char programSize[3];


   programSize[1] = fgetc ( incomingFile );

   programSize[0] = fgetc ( incomingFile );
   
   // Append the null character
   programSize[2] = '\0';

   char programSizeHex[3];
   stringToHex( programSizeHex, programSize, sizeof( programSize) - 1 );


   elf.elfProgramSize = strtol( programSizeHex, NULL, 16 );
   
   // Get the number of entries in the program header
   char entryProgram[3];

   entryProgram[1] = fgetc ( incomingFile );
   entryProgram[0] = fgetc ( incomingFile );
   
   
   // Append the null character
   entryProgram[2] = '\0';
 

   // Convert to hex
   char hexEntryProgram[3];

   stringToHex( hexEntryProgram, entryProgram, sizeof( entryProgram) - 1 );
   
   // Store in the struct
   elf.elfProgramHeaderNumber = strtol( hexEntryProgram, NULL, 16);
  


   // Get the Size of the section header
   char sectionSize[3];

   sectionSize[1] = fgetc ( incomingFile );
   sectionSize[0] = fgetc ( incomingFile );
   
   // Append the null character
   sectionSize[2] = '\0';

   // Convert to Hex
   char hexSectionSize[3];

   stringToHex( hexSectionSize, sectionSize, sizeof( sectionSize ) - 1 );

   //Store in the struct

   elf.elfSectionHeaderSize = strtol( hexSectionSize, NULL, 16);
   
   // Get the number of entries in the section header

   char sectionEntries[3];

   sectionEntries[1] = fgetc ( incomingFile );
   sectionEntries[0] = fgetc ( incomingFile );
   
   // Append the null character
   sectionEntries[2] = '\0';

   // Convert to Hex
   char hexSectionEntries[3];

   stringToHex( hexSectionEntries, sectionEntries, sizeof( sectionEntries ) - 1 );

   //Store in the struct

   elf.elfSectionHeaderEntries = strtol( hexSectionEntries, NULL, 16);
 


   // Get the index of the section header table
   char sectionIndex[3];

   sectionIndex[1] = fgetc ( incomingFile );
   sectionIndex[0] = fgetc ( incomingFile );
   
   // Append the null character
   sectionIndex[2] = '\0';

   // Convert to Hex
   char hexSectionIndex[3];

   stringToHex( hexSectionIndex, sectionIndex, sizeof( sectionIndex ) - 1 );

   //Store in the struct

   elf.elfIndexOfSection = strtol( hexSectionIndex, NULL, 16);
 

   // All information Stored in the Struct


   // Set the File pointer back to start
   fseek( incomingFile, 0, SEEK_SET );

   return elf;


} // End parse and store 32 function



// Takes an incoming file pointer and stores 
// the information into an ElfFile64 struct. 
// Definition of ElfFile64 is in readelf.h
ElfFile64 parseAndStore64 ( FILE* incomingFile ){
   
   ElfFile64 elf;

   // Make sure the file is at the start of the file
   fseek( incomingFile, 0, SEEK_SET );

   // For loop
   int i;

   // Get from file char by char
   char c = ' ';

   // String to hold the magic
   char magicFile[14];

   // Get the magic out of the file
   for ( i = 0; i < 13; i++){
      c = fgetc( incomingFile );
      magicFile[i] = c;
   }
   
   // Append the 0 char at the end
   magicFile[i] = '\0';

   // Save into the struct the string
   strcpy( elf.magic, magicFile);

   // Store the Class number which we know is 2
   // As this is a 64 bit file
   elf.classNum = 2;


   // Figure out if the file is big or little endian.
   if ( elf.magic[5] == 1 ){
      //Little Endian
      elf.littleEndian = 1;
   } else if (elf.magic[5] == 2 ) {
      // Big Endian

      elf.littleEndian = -1;
   } else {
      // Not Endian Type
      fprintf( stderr, "%s: [%d] is not a valid Endian type.\n", FILENAME, elf.magic[5]);
      
      exit ( EXIT_FAILURE );
   } // End Endian Check

   
   // Check the Version number, should always be 
   // 1.
   if ( elf.magic[6] != 1){
      // Not Verison 1, exit with failure
      fprintf( stderr, "%s: [%d] is not a valid verison.\n", FILENAME, elf.magic[6] );
      
      exit ( EXIT_FAILURE );
 
   }
   
   // Version is 1, set it in the struct.
   elf.version = 1;

   // OS/ABI Check
   // @TODO Write a function to return the correct value
   // for the struct.
   if ( elf.magic[7] == 0){

      // Store the identity as a string
      char identity[] = "Unix System-V";
      
      // Store in the Struct
      strcpy ( elf.identity, identity );
      
   } else if ( elf.magic[7] == 1){
      
      // Store the identity as a string
      char identity[] = "HP-UX";
      
      // Store in the Struct
      strcpy ( elf.identity, identity );

   } else {
             // @TODO add in the rest of the checks for the rest of the
             // systems.

      // Store the identity as a string
      char identity[] = "Unknown";
      
      // Store in the Struct
      strcpy ( elf.identity, identity );


   }// Only checking for the first 2 values



   // Check the ABI Version
   if ( elf.magic[8] != 0 ) {

      // Error Not allowed
      fprintf ( stderr, "%s: ABI supported version is only 0. Found [%d] instead.\n", FILENAME, elf.magic[8] );

      // Exit
      exit( EXIT_FAILURE );
   } 

   // ABI version is 0
   elf.elfABI = 0;

   // Get the next 4 bytes for the type
   c = ' ';

   // Holds the bytes for the type
   char type[5];
   for ( i = 0; i < 4; i++){
      c = fgetc( incomingFile );
      type[i] = c;
   }
   
   // Add the null character
   type[i] = '\0';

   char typeAsHex[ 5 ];

   stringToHex( typeAsHex, type, sizeof( type ) - 1 );


   // Type Check return in the struct
   typeCheck( typeAsHex, elf.elfType );
   
   // Get the Machine Type
   c = ' ';
   
   // Store the machine type
   char machine[3];

   for ( i = 0; i < 2; i++ ){
      c = fgetc( incomingFile );
      machine[i] = c;
   }
  
   // Append null character
   machine[i] = '\0';


   // Hold the converted type as a hex value
   char machineAsHex[3];

   // Convert to Hex
   stringToHex( machineAsHex, machine, sizeof(machine) - 1 );


   // Compare the machine as a hex value against the list of known
   // machines. Return in the struct.
   machineCheck( machineAsHex, elf.elfMachine );


   // Get the Version
   c = ' ';

   // Store the Version
   char version[5];
   int j = 2;
   for ( i = 0; i < 2; i++){
      c = fgetc( incomingFile );
      version[j] = c;
      j++;
      c = fgetc ( incomingFile);
      version[j] = c;
      j = j - 3;
   }

   version[4] = '\0';
   // Store the Hec version 
   char hexVersion[5];

   // convert the string
   stringToHex( hexVersion, version, sizeof( version) - 1 );
  
  
   char one[5] = {0x00, 0x00, 0x00, 0x01, 0x00};

   char hexOne[5];

   stringToHex ( hexOne, one, sizeof( one ) - 1 );


   // Compare the Strings if not equal then error
   if ( ( strcmp( hexOne, hexVersion) ) != 0 ){

      // If not Zero wrong version and go to next file
      fprintf( stderr, "%s: version [%s] not supported.\n", FILENAME, hexVersion );

      exit ( EXIT_FAILURE );
   }
   
   //Version is 1 store in struct
   elf.elfVersion = strtol( hexVersion, NULL, 16 );

   // Get the entry point
   c = ' ';
   i = 0;
   // Hold Entry point String]
   char entry[9];
   j = 6;

   // Clear the last 0
   c = fgetc( incomingFile );
   for ( i = 0; i < 4; i++){
      c = fgetc( incomingFile );
      entry[j] = c;
      j++;
      c = fgetc( incomingFile );
      entry[j] = c;
      j = j - 3;
   }
   
   // Append null character
   entry[8] = '\0';
   char hexEntry[9];

   stringToHex( hexEntry, entry, sizeof( entry ) - 1 );
   
   // Swap the order to match little endian 
   swap(hexEntry);

   // Store in the Struct
   strcpy( elf.elfEntry, hexEntry );

   // Get the Program header Start Address
   c = ' ';

   // Store the program header string
   char program[9];
   j = 6;
   for (i = 0; i < 4; i++ ){
      c = fgetc( incomingFile );
      program[j] = c;
      j++;
      c = fgetc ( incomingFile );
      program[j] = c;
      j = j - 3;

   }
   
   // Append the null character
   program[8] = '\0';

   // Hold the hex version of the string
   char hexProgram[9];

   stringToHex( hexProgram, program, sizeof( program ) - 1 );
   
   // Swap the order
   swap(hexProgram);
   // Store into the Struct
   strcpy( elf.elfProgram, hexProgram );


   // Get the program header start
   c = ' ';
   // Store the header  
   char header[9];
   j = 6;
   for ( i = 0; i < 4; i++){
      c = fgetc( incomingFile );
      header[j] = c;
      j++;
      c = fgetc( incomingFile );
      header[j] = c;
      j = j - 3;
   }
   // Append the null character
   header[8] = '\0';

   // Hold the header in hex
   char hexHeader[9];

   stringToHex( hexHeader, header, sizeof( header) - 1 ); 
   
   swap(hexHeader);
   
   // Store in the struct as a long long
   elf.elfHeader = strtoll( hexHeader, NULL, 16);

   // Get the Flags Section 
   c = ' ';
   // Store the Flags
   char flags[5];
   j = 2;
   for ( i = 0; i < 2; i++){
      c = fgetc ( incomingFile );
      flags[j] = c;
      j++;
      c = fgetc( incomingFile );
      flags[j] = c;
      j = j - 3;
   }  
   
   // Append the null character
   flags[4] = '\0';

   char hexFlags[5];

   // Convert to Hex
   stringToHex( hexFlags, flags, sizeof( flags ) - 1 );

   // Store in the struct
   strcpy( elf.elfFlags, hexFlags );   

   // Get the Size of header should be 0x40
   c = ' ';
   char headSize[3];
   headSize[1] = fgetc ( incomingFile);

   headSize[0] = fgetc ( incomingFile);

   // Append the 0 character
   headSize[2] = '\0';
   

   
   // Convert to Hex Store in the Struct
   stringToHex( elf.elfSize, headSize, sizeof( headSize ) - 1 );
   
   
   // Get the Program Header Size
   c = ' ';
   char programSize[3];


   programSize[1] = fgetc ( incomingFile );

   programSize[0] = fgetc ( incomingFile );
   
   // Append the null character
   programSize[2] = '\0';

   char programSizeHex[3];
   stringToHex( programSizeHex, programSize, sizeof( programSize) - 1 );


   elf.elfProgramSize = strtol( programSizeHex, NULL, 16 );
   
   // Get the number of entries in the program header
   char entryProgram[3];

   entryProgram[1] = fgetc ( incomingFile );
   entryProgram[0] = fgetc ( incomingFile );
   
   
   // Append the null character
   entryProgram[2] = '\0';
 

   // Convert to hex
   char hexEntryProgram[3];

   stringToHex( hexEntryProgram, entryProgram, sizeof( entryProgram) - 1 );
   
   // Store in the struct
   elf.elfProgramHeaderNumber = strtol( hexEntryProgram, NULL, 16);
  


   // Get the Size of the section header
   char sectionSize[3];

   sectionSize[1] = fgetc ( incomingFile );
   sectionSize[0] = fgetc ( incomingFile );
   
   // Append the null character
   sectionSize[2] = '\0';

   // Convert to Hex
   char hexSectionSize[3];

   stringToHex( hexSectionSize, sectionSize, sizeof( sectionSize ) - 1 );

   //Store in the struct

   elf.elfSectionHeaderSize = strtol( hexSectionSize, NULL, 16);
   
   // Get the number of entries in the section header

   char sectionEntries[3];

   sectionEntries[1] = fgetc ( incomingFile );
   sectionEntries[0] = fgetc ( incomingFile );
   
   // Append the null character
   sectionEntries[2] = '\0';

   // Convert to Hex
   char hexSectionEntries[3];

   stringToHex( hexSectionEntries, sectionEntries, sizeof( sectionEntries ) - 1 );

   //Store in the struct

   elf.elfSectionHeaderEntries = strtol( hexSectionEntries, NULL, 16);
 


   // Get the index of the section header table
   char sectionIndex[3];

   sectionIndex[1] = fgetc ( incomingFile );
   sectionIndex[0] = fgetc ( incomingFile );
   
   // Append the null character
   sectionIndex[2] = '\0';

   // Convert to Hex
   char hexSectionIndex[3];

   stringToHex( hexSectionIndex, sectionIndex, sizeof( sectionIndex ) - 1 );

   //Store in the struct

   elf.elfIndexOfSection = strtol( hexSectionIndex, NULL, 16);
 

   // All information Stored in the Struct


   // Set the File pointer back to start
   fseek( incomingFile, 0, SEEK_SET );

   return elf;


} // End parse and store 64 function




// Checks which type, if any, the incoming string 
// matches. Returns in the second string.
void typeCheck ( char* hexString, char* returnString ) {

   // String with 00's and 0 terminated
   char checkZeroString[] = { 0x00, 0x00, 0x00, 0x00, 0x00 };
   
   // Holds the string to be checked against incoming hex String
   char hexCheckString[6];

   stringToHex( hexCheckString, checkZeroString, sizeof(checkZeroString) - 1  );
   

   // Check which, if any, type the string is. If
   // none then error and exit.
   // Types found via wikipedia
   // Check if it is zero.
   if ( ( strcmp(hexString, hexCheckString) ) == 0 ){
      
      
      char type[60] = "NONE";
      
      // Copy to the the return String
      strcpy ( returnString, type );


      //return back to main function
      return;
   }

   char checkOneString[] = { 0x00, 0x00, 0x00, 0x01, 0x00 };


   stringToHex( hexCheckString, checkOneString, sizeof(checkOneString) - 1  );
      
   // Check if it is One
   if ( ( strcmp( hexString, hexCheckString ) ) == 0 ){ 

      char type[60] = "REL ( Relocatable File )";
      
      // Copy to the the return String
      strcpy ( returnString, type );


      //return back to main function
      return;   
   
   } 

   char checkTwoString[] = { 0x00, 0x00, 0x00, 0x02, 0x00 };
   
   stringToHex( hexCheckString, checkTwoString, sizeof(checkTwoString) - 1  );

   // Check if it is two
   if ( ( strcmp( hexString, hexCheckString ) ) == 0 ){ 
 
      char type[60] = "EXEC ( Executable File )";
      
      // Copy to the the return String
      strcpy ( returnString, type );

      //return back to main function
      return;
   
   }

   char checkThreeString[] = { 0x00, 0x00, 0x00, 0x03, 0x00 };
   
   stringToHex( hexCheckString, checkThreeString, sizeof(checkThreeString) - 1  );

   // Check if it is three
   if ( ( strcmp( hexString, hexCheckString ) ) == 0 ){ 

      char type[60] = "DYN ( Position-Independant Executable File )";
      
      // Copy to the the return String
      strcpy ( returnString, type );


      //return back to main function
      return;
 
   } 
   
   char checkFourString[] = { 0x00, 0x00, 0x00, 0x04, 0x00 };
   
   stringToHex( hexCheckString, checkFourString, sizeof(checkFourString) - 1  );

   // Check if it is Four
   if ( ( strcmp(hexString, hexCheckString ) ) == 0 ){ 

      char type[60] = "CORE ( Core Dump File )";
      
      // Copy to the the return String
      strcpy ( returnString, type );

      //return back to main function
      return;
   } 


   // Check for the operating System independant Ranges
   if ( ( *hexString >= 0xFE00 ) && ( *hexString <= 0xFEFF ) ){ 

      char type[60] = "OP ( Operating System Dependant )";
      
      // Copy to the the return String
      strcpy ( returnString, type );


      //return back to main function
      return;

   }


   // Check for the Processor independant Ranges
   if ( ( *hexString >= 0xFF00 ) && ( *hexString <= 0xFFFF ) ){ 

      char type[60] = "CPU ( CPU Dependant )";

      // Copy to the the return String
      strcpy ( returnString, type );

      //return back to main function
      return;


   } else {
      // No matches at all exit

      fprintf( stderr, "%s: type [%s] is unknown.\n", FILENAME, hexString );

      // Exit
      exit( EXIT_FAILURE );

   }


}// End typeCheck function


// Checks which type, if any, the incoming string 
// matches. Prints result to the screen.
// @TODO Add in all Machine Types only 5 are
// supported at this time.
void machineCheck ( char* hexString, char* returnString ) {

   // String with 00's and 0 terminated
   char noSpecific[] = { 0x00, 0x00, 0x00 };
   
   // Holds the string to be checked against incoming hex String
   char hexCheckString[3];

  
   stringToHex( hexCheckString, noSpecific, sizeof( noSpecific ) - 1  );

   // Check No Specific Instruction Set
   if ( ( strcmp( hexCheckString, hexString) ) == 0 ){
      
      
      // String for this match
      char machine[60] = "No Specific Machine";
      
      //Store in the return string
      strcpy( returnString, machine );
   
      //return back to main function
      return;


   } // End No Specific Instruction Set


   char aTAndT[] = { 0x00, 0x01, 0x00 };


   stringToHex( hexCheckString, aTAndT, sizeof( aTAndT ) - 1  );

   // Check AT&T 32100
   if ( ( strcmp( hexCheckString, hexString) ) == 0 ){
      
      
      // String for this match
      char machine[60] = "AT&T WE 32100";
      
      //Store in the return string
   
      strcpy( returnString, machine );
      //return back to main function
      return;

   }

   char SPARC[] = { 0x00, 0x02, 0x00 };


   stringToHex( hexCheckString, SPARC, sizeof( SPARC ) - 1  );
   
   // Check SPARC
   if ( ( strcmp( hexCheckString, hexString) ) == 0 ){
      char machine[60] = "SPARC";
      
      //Store in the return string
      strcpy( returnString, machine );
   
      //return back to main function
      return;
   
   }

   char x86[] = { 0x00, 0x03, 0x00 };


   stringToHex( hexCheckString, x86, sizeof( x86 ) - 1  );
   
   // Check x86
   if ( ( strcmp( hexCheckString, hexString) ) == 0 ){
      
         // String for this match
      char machine[60] = "x86";
      
      //Store in the return string
      strcpy( returnString, machine );
      //return back to main function
      return;
   
   }

   char motor68[] = { 0x00, 0x04, 0x00 };


   stringToHex( hexCheckString, motor68, sizeof( motor68 ) - 1  );

   // Check motor 68000
   if ( ( strcmp( hexCheckString, hexString) ) == 0 ){
      
         // String for this match
      char machine[60] = "Motorola 68000";
      
      //Store in the return string
      strcpy( returnString, machine );
      //return back to main function
      return;

   }

   char amd86[] = { 0x00, 0x3e, 0x00 };


   stringToHex( hexCheckString, amd86, sizeof( amd86 ) - 1  );

   // Check AMD x86
   if ( ( strcmp( hexCheckString, hexString) ) == 0 ){
      
      // String for this match
      char machine[60] = "Advanced Micro Device X86-64";
      
      //Store in the return string
      strcpy( returnString, machine );
      
      //return back to main function
      return;

   }


   // @TODO add in other checks here
   // String for this match
   char machine[60] = "Unknown";
      
   //Store in the return string
   strcpy( returnString, machine );
   
   return;




} // End Type Check Function
  
  

// Prints Out all the information about a 64 bit Struct
void print64( ElfFile64* incomingElf ){

   printf("ELF Header: \n");

   int i = 0;
   
   // Print Magic
   printf( " Magic: \t");
   for( i = 0; i < 16; i++ ){
     
      // Pad out with 0's as string is not always 0 at the
      // end.
      if ( i >= 7){
         printf("00 ");
      } else {

      printf( "%02x ", incomingElf->magic[i] );

      }

   } // Magic Printed

   printf("\n");

   printf(" Class: \t\t\t\t ELF64\n" );
   

   if ( incomingElf->littleEndian == 1){
      // Little Endian
      printf(" Data: \t\t\t\t\t 2's compliment, little endian\n" );

   } else {
      // Big Endian
      printf(" Data: \t\t\t\t\t 2's compliment, big endian\n" );
   
   }

   if ( incomingElf->version == 1) {
      // Right Version
      printf( " Version: \t\t\t\t 1 ( current )\n");

   } else {
      // Wrong Version
      fprintf( stderr, "%s: Version [%d] not supported. Only Version 1.\n", FILENAME, incomingElf->version );
      exit( EXIT_FAILURE);
   }

   printf(" OS/ABI:  \t\t\t\t %s\n",                         incomingElf->identity );
   

   if ( incomingElf->elfABI != 0 ) {
      // Wrong ABI Version
      fprintf( stderr, "%s: ABI [%d] not supported. Only ABI 0.\n", FILENAME, incomingElf->elfABI );
      exit( EXIT_FAILURE);
 
   }

   printf(" ABI Version:  \t\t\t\t %d\n",                         incomingElf->elfABI );

   printf(" Type:   \t\t\t\t %s\n",                                incomingElf->elfType);
   
   printf(" Machine: \t\t\t\t %s\n",                       incomingElf->elfMachine);
   
   printf(" Version: \t\t\t\t 0x%ld\n",                       incomingElf->elfVersion);
   
   // Remove Leading 0's
   for ( i = 0; i < strlen( incomingElf->elfEntry); i++ ) {
      if ( incomingElf->elfEntry[i] != 0x30){
         break;
      }
   }
   printf(" Entry point of Address: \t\t 0x");
   // i is now pointing to the first non 0 char
   // print out the rest of the chars
   for( ; i < strlen( incomingElf->elfEntry); i++ ){
      printf("%c", incomingElf->elfEntry[i] );

   }
   printf("\n");

   
   printf(" Start of the program Headers: \t\t %ld (bytes into the file)\n",          strtol( incomingElf->elfProgram, NULL, 16 ) );
   printf(" Start of the section Headers: \t\t %lld\n",                      incomingElf->elfHeader);
   printf(" Flags: \t\t\t\t 0x%ld\n",                       strtol( incomingElf->elfFlags, NULL, 16 ) );
   printf(" Size of this header: \t\t\t %ld (bytes)\n",               strtol( incomingElf->elfSize, NULL, 16 ) );
   printf(" Size of Program Header: \t\t %ld (bytes)\n",                 incomingElf->elfProgramSize);
   printf(" Number of Program Headers: \t\t %ld\n",        incomingElf->elfProgramHeaderNumber);
   printf(" Size of section Headers: \t\t %ld (bytes)\n",          incomingElf->elfSectionHeaderSize);
   printf(" Number of Section Headers: \t\t %ld\n",       incomingElf->elfSectionHeaderEntries );
   printf(" Section Header string table index: \t %ld\n",             incomingElf->elfIndexOfSection);





} // End print64 function
  
// Prints Out all the information about a 64 bit Struct
void print32( ElfFile32* incomingElf ){

   printf("ELF Header: \n");

   int i = 0;
   
   // Print Magic
   printf( " Magic: \t");
   for( i = 0; i < 16; i++ ){
     
      // Pad out with 0's as string is not always 0 at the
      // end.
      if ( i >= 7){
         printf("00 ");
      } else {

      printf( "%02x ", incomingElf->magic[i] );

      }

   } // Magic Printed

   printf("\n");

   printf(" Class: \t\t\t\t ELF64\n" );
   

   if ( incomingElf->littleEndian == 1){
      // Little Endian
      printf(" Data: \t\t\t\t\t 2's compliment, little endian\n" );

   } else {
      // Big Endian
      printf(" Data: \t\t\t\t\t 2's compliment, big endian\n" );
   
   }

   if ( incomingElf->version == 1) {
      // Right Version
      printf( " Version: \t\t\t\t 1 ( current )\n");

   } else {
      // Wrong Version
      fprintf( stderr, "%s: Version [%d] not supported. Only Version 1.\n", FILENAME, incomingElf->version );
      exit( EXIT_FAILURE);
   }

   printf(" OS/ABI:  \t\t\t\t %s\n",                         incomingElf->identity );
   

   if ( incomingElf->elfABI != 0 ) {
      // Wrong ABI Version
      fprintf( stderr, "%s: ABI [%d] not supported. Only ABI 0.\n", FILENAME, incomingElf->elfABI );
      exit( EXIT_FAILURE);
 
   }

   printf(" ABI Version:  \t\t\t\t %d\n",                         incomingElf->elfABI );

   printf(" Type:   \t\t\t\t %s\n",                                incomingElf->elfType);
   
   printf(" Machine: \t\t\t\t %s\n",                       incomingElf->elfMachine);
   
   printf(" Version: \t\t\t\t 0x%ld\n",                       incomingElf->elfVersion);
   
   // Remove Leading 0's
   for ( i = 0; i < strlen( incomingElf->elfEntry); i++ ) {
      if ( incomingElf->elfEntry[i] != 0x30){
         break;
      }
   }
   printf(" Entry point of Address: \t\t 0x");
   // i is now pointing to the first non 0 char
   // print out the rest of the chars
   for( ; i < strlen( incomingElf->elfEntry); i++ ){
      printf("%c", incomingElf->elfEntry[i] );

   }
   printf("\n");

   
   printf(" Start of the program Headers: \t\t %ld (bytes into the file)\n",          strtol( incomingElf->elfProgram, NULL, 16 ) );
   printf(" Start of the section Headers: \t\t %lld\n",                      incomingElf->elfHeader);
   printf(" Flags: \t\t\t\t 0x%ld\n",                       strtol( incomingElf->elfFlags, NULL, 16 ) );
   printf(" Size of this header: \t\t\t %ld (bytes)\n",               strtol( incomingElf->elfSize, NULL, 16 ) );
   printf(" Size of Program Header: \t\t %ld (bytes)\n",                 incomingElf->elfProgramSize);
   printf(" Number of Program Headers: \t\t %ld\n",        incomingElf->elfProgramHeaderNumber);
   printf(" Size of section Headers: \t\t %ld (bytes)\n",          incomingElf->elfSectionHeaderSize);
   printf(" Number of Section Headers: \t\t %ld\n",       incomingElf->elfSectionHeaderEntries );
   printf(" Section Header string table index: \t %ld\n",             incomingElf->elfIndexOfSection);





} // End print32 function
 
